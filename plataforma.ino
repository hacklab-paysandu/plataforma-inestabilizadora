#include <Servo.h>

#define INPUT_1     A0
#define INPUT_2     A1

#define INPUT_3     2

#define SERVO_1     4
#define SERVO_2     3

#define LED_1       8
#define LED_2       7
#define LED_3       6

#define ANGULO      45

#define M_PI        3.14159

#define LIBRE       0
#define HORIZONTAL  1
#define FRONTAL     2

#define CADA      50

Servo s1;
Servo s2;

long antes = 0;
int i = 0;
int opc = LIBRE;

int t = 0;

void setup ()
{
  Serial.begin( 9600 );

  pinMode( INPUT_1, INPUT );
  pinMode( INPUT_2, INPUT );
  
  pinMode( INPUT_3, INPUT );
  attachInterrupt( digitalPinToInterrupt( INPUT_3 ), cambiar, RISING );

  s1.attach( SERVO_1 );
  s2.attach( SERVO_2 );

  s1.write( 90 );
  s2.write( 90 );
  
  antes = millis();
  
  digitalWrite( LED_1, HIGH );
  digitalWrite( LED_2, LOW );
  digitalWrite( LED_3, LOW );

  t = millis();
}

void cambiar ()
{
  if ( millis() - t < 1000 )
    return;

  t = millis();
  
  opc++;

  if ( opc == 3 )
    opc = 0;

  if ( opc == 0 )
  {
    digitalWrite( LED_1, HIGH );
    digitalWrite( LED_2, LOW );
    digitalWrite( LED_3, LOW );
  }
  else if ( opc == 1 )
  {
    digitalWrite( LED_1, LOW );
    digitalWrite( LED_2, HIGH );
    digitalWrite( LED_3, LOW );
  }
  else if ( opc == 2 )
  {
    digitalWrite( LED_1, LOW );
    digitalWrite( LED_2, LOW );
    digitalWrite( LED_3, HIGH );
  }

  i = 0;
  antes = millis();
}

#define MAX   2
#define MED   1
#define MIN   0.3

void loop ()
{
  if ( opc == LIBRE )
  {
    int i1 = map( analogRead( INPUT_1 ), 0, 1023, ANGULO, -ANGULO);
    int i2 = map( analogRead( INPUT_2 ), 0, 1023, ANGULO, -ANGULO);

    mover( i1, i2 );
  }
  else
  {
    while ( millis() - antes <= CADA );
    antes = millis();

    int in2 = 1023 - analogRead( INPUT_2 );
    double cant = MAX;

    if ( in2 < 341 )
      cant = MIN;
    else if ( in2 < 682 )
      cant = MED;
    
    double a = sin( ( i * cant ) * ( PI / ( 1000 / CADA ) ) ) * ANGULO;
    
    if ( opc == HORIZONTAL )
      mover( a, 0 );
    else if ( opc == FRONTAL )
      mover( 0, a );
    
    i += 1;
  }
}

/* variar
 *  
 *  eje: Eje en que se varia el movimiento
 *  cantidad: Cantidad de oscilaciones por segundo
 *  tiempo: Cantidad de tiempo por la que se oscilará
 * */
void variar ( int eje, double cantidad, double tiempo )
{
  int t = tiempo * 1000;
  long tInicial = millis();
  cantidad *= 2;

  int i = 0;
  
  long antes = millis();
  while ( millis() - tInicial <= t )
  {
    while ( millis() - antes <= CADA );
    antes = millis();

    double a = sin( ( i * cantidad ) * ( PI / ( 1000 / CADA ) ) ) * ANGULO;

    if ( eje == HORIZONTAL )
      mover( a, 0 );
    else if ( eje == FRONTAL )
      mover( 0, a );
    
    i += 1;
  }

  mover( 0, 0 );
}

void mover ( int v1, int v2 )
{
  int largo = sqrt( pow( v1, 2 ) + pow( v2, 2 ) );
  double ang = atan2( v1, v2 ) + M_PI -  ( M_PI / 4 );

  int nv1 = sin( ang ) * largo;
  int nv2 = cos( ang ) * largo;

  s1.write( 90 + nv1 );
  s2.write( 90 + nv2 );
}
